package asf.testing.practica;

import java.util.*;

public class OfertaHotelera {

    private List<Hotel> hoteles;

    /*
    * Constructor que inicializa la clase a partir de la lista de Hoteles recibida como parámetro
    * */
    public OfertaHotelera(List<Hotel> hoteles) {
        this.hoteles = new ArrayList<>(hoteles);
    }

    /*
    * Devuelve todos los hoteles almacenados en una única colección de tipo List.
    * En caso de no haber hoteles deberá devolver una colección vacía.
    * */
    public List<Hotel> obtenerHoteles () {
        return this.hoteles;
    }

    /**
     * Obtiene un Mapa de los hoteles teniendo como clave la Provincia
     */
    public Map<Provincia, List<Hotel>> obtenerHotelesPorProvincia() {
        Map<Provincia, List<Hotel>> mapaProvinciasConListaHoteles = new HashMap<>();
        for( Hotel hotel : this.hoteles) {
            Provincia provinciaDelHotel = hotel.getCiudad().getProvincia();
            if (!mapaProvinciasConListaHoteles.containsKey(provinciaDelHotel))
                mapaProvinciasConListaHoteles.put(provinciaDelHotel, new ArrayList<Hotel>());
            mapaProvinciasConListaHoteles.get(provinciaDelHotel).add(hotel);
        }
        return mapaProvinciasConListaHoteles;
    }

    /*
    * Obtiene una única colección de tipo List con los hoteles pertenecientes a una Ciudad dada
    * En caso de no existir ningún hotel para la ciudad dada, deberá devolver una lista vacía.
    * */
    public List<Hotel> obtenerHotelesDeUnaCiudad(Ciudad ciudad) {
        List<Hotel> listaDeHotelesEnCiudad = new ArrayList<>();
        for(Hotel hotel : this.hoteles)
            if(hotel.getCiudad().getId() == ciudad.getId())
                listaDeHotelesEnCiudad.add(hotel);
        return listaDeHotelesEnCiudad;
    }

    /*
    * Obtiene un único Hotel por su Id
    * En caso de no existir el Id, deberá devolver una Excepción NoSuchElementException
    *   con el mensaje "Id de Hotel no válido"
    * */
    public Hotel obtenerHotelPorId (Integer id) {
        for(Hotel hotel : this.hoteles)
            if(hotel.getId() == id)
              return hotel;
        throw new NoSuchElementException("Id de Hotel no válido");
    }

    /*
    * Permite añadir un hotel a la colección
    * */
    public void anyadirHotel(Hotel hotel){
        this.hoteles.add(hotel);
    }

}
