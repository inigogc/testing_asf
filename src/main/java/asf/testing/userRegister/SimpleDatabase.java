package asf.testing.userRegister;


import java.util.ArrayList;
import java.util.List;

public class SimpleDatabase implements Database {

    private List<User> users = new ArrayList<>();

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public boolean hasUser(User user) {
        return users.contains(user);
    }

    @Override
    public void deleteUser(User user) throws UserNotFoundException {
        if (!hasUser(user)) {
            throw new UserNotFoundException();
        }
        users.remove(user);
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public int numberOfUsers() {
        return users.size();
    }

    @Override
    public boolean isReadWriteSupported() {
        return true;
    }

    @Override
    public String getDatabaseName() {
        return "SimpleDatabase";
    }

}
