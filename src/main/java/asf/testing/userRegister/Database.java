package asf.testing.userRegister;

import java.util.List;

public interface Database {

    void addUser(User user);

    boolean hasUser(User user);

    void deleteUser(User user) throws UserNotFoundException;

    List<User> getUsers();

    int numberOfUsers();

    boolean isReadWriteSupported();

    String getDatabaseName();
}
