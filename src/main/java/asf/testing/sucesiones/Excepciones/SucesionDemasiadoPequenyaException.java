package asf.testing.sucesiones.Excepciones;

public class SucesionDemasiadoPequenyaException extends Exception{
    public SucesionDemasiadoPequenyaException(String message) {
        super(message);
    }
}
