package asf.testing.sucesiones.Excepciones;

public class ElementoNoExisteException extends Exception {
    public ElementoNoExisteException(String message) {
        super(message);
    }
}
