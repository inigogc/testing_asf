package asf.testing.practica;

import asf.testing.practica.Ciudad;
import asf.testing.practica.Empresa;
import asf.testing.practica.Hotel;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class HotelTest {

    @Test
    public void elPrimerHotelCreadoTieneId0() {
        Ciudad ciudad = Mockito.mock(Ciudad.class);
        Empresa empresa = Mockito.mock(Empresa.class);
        Hotel primerHotelCreado = new Hotel(ciudad, empresa, "Calle Falsa 1", 3);

        Integer idDelPrimerHotel = primerHotelCreado.getId();

        assertThat(idDelPrimerHotel, equalTo(0));
    }
}
