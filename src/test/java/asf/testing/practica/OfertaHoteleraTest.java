package asf.testing.practica;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

public class OfertaHoteleraTest {

    Hotel hotel1, hotel2, hotel3;
    List<Hotel> listaHotelesParaInicializar = new ArrayList<>();

    @Before
    public void initHoteles() {
        this.hotel1 = Mockito.mock(Hotel.class);
        this.hotel2 = Mockito.mock(Hotel.class);
        this.hotel3 = Mockito.mock(Hotel.class);
    }

    private void anyadirHotelesAListaParaInicializar() {
        this.listaHotelesParaInicializar.add(this.hotel1);
        this.listaHotelesParaInicializar.add(this.hotel2);
        this.listaHotelesParaInicializar.add(this.hotel3);
    }

    @Test(expected = NullPointerException.class)
    public void laInicializaciónConUnNullCausaNullPointerException() {

        OfertaHotelera ofertaHotelera = new OfertaHotelera(null);

    }

    @Test
    public void siNoHayHotelesSeDevuelveUnaListaVacia() {
        OfertaHotelera ofertaHoteleraSinHoteles = new OfertaHotelera(listaHotelesParaInicializar);

        List<Hotel> listaObtenida = ofertaHoteleraSinHoteles.obtenerHoteles();

        assertThat("La lista debe devolverse y no contener ningún hotel.",
                listaObtenida, empty());
    }

    @Test
    public void obtenerHotelesDevuelveLosHotelesInicializados() {
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHoteleraSinHoteles = new OfertaHotelera(listaHotelesParaInicializar);

        List<Hotel> listaObtenida = ofertaHoteleraSinHoteles.obtenerHoteles();

        assertThat("Una lista inicializada con los tres hoteles de ejemplo debe contenerlos en la devuelta.",
                listaObtenida, contains(hotel1, hotel2, hotel3));
    }

    @Test
    public void obtenerHotelesDevuelveLosHotelesInicializadosYAnadidos() {
        listaHotelesParaInicializar.add(hotel1);
        listaHotelesParaInicializar.add(hotel2);
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        ofertaHotelera.anyadirHotel(hotel3);

        List<Hotel> listaHotelesObtenida = ofertaHotelera.obtenerHoteles();

        assertThat("La lista debe contener los dos hoteles de la inicialización y el añadido mediante el método.",
                listaHotelesObtenida, contains(hotel1, hotel2, hotel3));
    }

    @Test
    public void unaModificacionSobreLaListaDeObtenerHotelesModificaLaListaDeLaInstancia() {
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Hotel nuevoHotel = Mockito.mock(Hotel.class);
        List<Hotel> listaObtenida;

        listaObtenida = ofertaHotelera.obtenerHoteles();
        ofertaHotelera.anyadirHotel(nuevoHotel);

        assertThat("La lista obtenida de hoteles contiene el hotel añadido a posteriori.",
                listaObtenida, hasItem(nuevoHotel));
    }

    @Test
    public void obtenerHotelesPorProvinciadevuelveUnParProvinicaListaHoteles() {
        Ciudad ciudad1 = Mockito.mock(Ciudad.class);
        Ciudad ciudad2 = Mockito.mock(Ciudad.class);
        Provincia provincia1 = Mockito.mock(Provincia.class);
        Provincia provincia2 = Mockito.mock(Provincia.class);
        when(hotel1.getCiudad()).thenReturn(ciudad1);
        when(hotel2.getCiudad()).thenReturn(ciudad1);
        when(hotel3.getCiudad()).thenReturn(ciudad2);
        when(ciudad1.getProvincia()).thenReturn(provincia1);
        when(ciudad2.getProvincia()).thenReturn(provincia2);
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        List<Hotel> listaDeHotelesEsperadaParaProvincia1 = new ArrayList<>();
        listaDeHotelesEsperadaParaProvincia1.add(hotel1);
        listaDeHotelesEsperadaParaProvincia1.add(hotel2);

        Map<Provincia, List<Hotel>> mapaProvinciasListaHoteles = ofertaHotelera.obtenerHotelesPorProvincia();

        assertThat("El mapa de debe coincidir con el par Provincia-Lista de hoteles esperado.",
                mapaProvinciasListaHoteles, IsMapContaining.hasEntry(provincia1, listaDeHotelesEsperadaParaProvincia1));
    }

    @Test
    public void unaProvinciaSinHotelesdevuelveMapaDeTamano0() {
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);

        Map<Provincia, List<Hotel>> mapaProvinciasListaHoteles = ofertaHotelera.obtenerHotelesPorProvincia();

        assertThat("Una OfertaHotelera sin hoteles devuelve un mapa de tamaño cero.",
                mapaProvinciasListaHoteles.size(), is(0));
    }

    @Test
    public void obtenerHotelesDeUnaCiudadDevuelveUnaListaConLosHotelesEsperados() {
        Ciudad ciudadParaObtenerHoteles = Mockito.mock(Ciudad.class);
        Ciudad ciudadDeControl = Mockito.mock(Ciudad.class);
        when(hotel1.getCiudad()).thenReturn(ciudadParaObtenerHoteles);
        when(hotel2.getCiudad()).thenReturn(ciudadParaObtenerHoteles);
        when(hotel3.getCiudad()).thenReturn(ciudadDeControl);
        when(ciudadParaObtenerHoteles.getId()).thenReturn(0);
        when(ciudadDeControl.getId()).thenReturn(1);
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        List<Hotel> listaDeHotelesEsperados = new ArrayList<>();
        listaDeHotelesEsperados.add(hotel1);
        listaDeHotelesEsperados.add(hotel2);

        List<Hotel> listaDeHotelesObtenida = ofertaHotelera.obtenerHotelesDeUnaCiudad(ciudadParaObtenerHoteles);

        assertThat("Una lista de hoteles de una ciudad debe devolver solo los hoteles de esa ciudad.",
                listaDeHotelesObtenida, equalTo(listaDeHotelesEsperados));
    }

    @Test
    public void obtenerHotelesDeUnaCiudadDevuelveUnaListaVaciaSiNoHayHoteles() {
        Ciudad ciudad1 = Mockito.mock(Ciudad.class);
        Ciudad ciudad2 = Mockito.mock(Ciudad.class);
        when(hotel1.getCiudad()).thenReturn(ciudad1);
        when(hotel2.getCiudad()).thenReturn(ciudad1);
        when(hotel3.getCiudad()).thenReturn(ciudad2);
        when(ciudad1.getId()).thenReturn(0);
        when(ciudad2.getId()).thenReturn(1);
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Ciudad ciudadSinHoteles = Mockito.mock(Ciudad.class);
        when(ciudadSinHoteles.getId()).thenReturn(2);

        List<Hotel> listaObtenida = ofertaHotelera.obtenerHotelesDeUnaCiudad(ciudadSinHoteles);

        assertThat("Una ciudad sin hoteles debe devolver una lista vacía.",
                listaObtenida, empty());
    }

    @Test
    public void obtenerHotelPorIdDevuelveElHotelConEsaId() {
        when(hotel1.getId()).thenReturn(0);
        when(hotel2.getId()).thenReturn(1);
        when(hotel3.getId()).thenReturn(2);
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Integer idDelHotelSolicitado = 1;

        Integer idDelHotelDevuelto = ofertaHotelera.obtenerHotelPorId(idDelHotelSolicitado).getId();

        assertThat("Al obtener un hotel con una ID, el hotel recibido contiene la ID esperada.",
                idDelHotelSolicitado, equalTo(idDelHotelDevuelto));
    }

    @Test(expected = NoSuchElementException.class)
    public void obtenerHotelPorIdInexistenteDevuelveExcepcion() {
        when(hotel1.getId()).thenReturn(0);
        when(hotel2.getId()).thenReturn(1);
        when(hotel3.getId()).thenReturn(2);
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Integer idDelHotelSolicitado = 5;

        Integer idDelHotelDevuelto = ofertaHotelera.obtenerHotelPorId(idDelHotelSolicitado).getId();

    }

    @Test
    public void anyadirUnHotelIncluyeEseHotelEnLaListaDeHotelesDevuelta() {
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Hotel hotelAnyadido = Mockito.mock(Hotel.class);

        ofertaHotelera.anyadirHotel(hotelAnyadido);
        List<Hotel> listaHotelesObtenida = ofertaHotelera.obtenerHoteles();

        assertThat("Un hotel añadido tras la inicialización constará en la lista devuelta.",
                listaHotelesObtenida, hasItem(hotelAnyadido));
    }

    @Test
    public void anyadirUnHotelAumentaElTamanyoDeLaListaDevuelta() {
        anyadirHotelesAListaParaInicializar();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesParaInicializar);
        Hotel nuevoHotel = Mockito.mock(Hotel.class);
        List<Hotel> listaObtenidaSinNuevoHotel = new ArrayList<>();
        List<Hotel> listaObtenidaConNuevoHotel = new ArrayList<>();

        listaObtenidaSinNuevoHotel.addAll(ofertaHotelera.obtenerHoteles());
        ofertaHotelera.anyadirHotel(nuevoHotel);
        listaObtenidaConNuevoHotel.addAll(ofertaHotelera.obtenerHoteles());

        assertThat("La lista con nuevos hoteles deberá tener un mayor tamaño que la recibida tras la inicialización.",
                listaObtenidaConNuevoHotel.size(), greaterThan(listaObtenidaSinNuevoHotel.size()));
    }

    @Test
    public void anyadirUnHotelRepetidoEsValido() {
        List<Hotel> listaHotelesInicial = new ArrayList<>();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(listaHotelesInicial);
        Hotel hotelRepetido = Mockito.mock(Hotel.class);

        ofertaHotelera.anyadirHotel(hotelRepetido);
        ofertaHotelera.anyadirHotel(hotelRepetido);
        List<Hotel> listaDeHotelesObtenida = ofertaHotelera.obtenerHoteles();

        assertThat("Si el hotel logra añadirse 2 veces, el tamaño de la lista debe ser 2.",
                listaDeHotelesObtenida, hasSize(2));
    }
}
