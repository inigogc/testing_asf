package asf.testing.practica;

import asf.testing.practica.Provincia;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.is;

public class ProvinciaTest {

    @Test
    public void laPrimeraProvinciaTieneElId0() {
        Provincia primeraProvincia = new Provincia("Soria", 8);

        int idPrimeraProvincia = primeraProvincia.getId();

        assertThat(idPrimeraProvincia, equalTo(0));
    }

    @Test
    public void unaProvinciaConNombreSoriaDevuelveElNombreSoria() {
        String parametroDeEntradaNombre = "Soria";
        Provincia provinciaDeNombreSoria = new Provincia(parametroDeEntradaNombre, 1);

        String resultadoGetNombre = provinciaDeNombreSoria.getNombre();

        assertThat(resultadoGetNombre, equalTo("Soria"));
    }

    @Test
    public void unaProvinciaConNombreNuloDevuelveComoNombreNulo() {
        String parametroDeEntradaNombre = null;
        Provincia provinciaConNombreNulo = new Provincia(null, 1);

        String resultadoGetNombre = provinciaConNombreNulo.getNombre();

        assertThat(resultadoGetNombre, is(nullValue()));
    }

    @Test
    public void unaProvinciaALaQueSeCambiaElNombreDevuelveElNuevoNombre() {
        String primerNombre = "Soria";
        String nuevoNombre = "Palencia";
        Provincia provincia = new Provincia(primerNombre, 1);

        provincia.setNombre(nuevoNombre);
        String nombreDeLaProvincia = provincia.getNombre();

        assertThat(nombreDeLaProvincia, equalTo(nuevoNombre));
    }
}
