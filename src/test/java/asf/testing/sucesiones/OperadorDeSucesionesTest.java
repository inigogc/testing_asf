package asf.testing.sucesiones;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class OperadorDeSucesionesTest {

    @Test
    public void dosSucesionesConMediaYSumatorioIgualesDevuelveTrue() {

        Sucesion sucesion1 = Mockito.mock(Sucesion.class);
        when(sucesion1.mediaDeLaSucesion()).thenReturn(new Float(107));
        when(sucesion1.sumatorioSucesion()).thenReturn(new Integer(1500));
        Sucesion sucesion2 = Mockito.mock(Sucesion.class);
        when(sucesion2.mediaDeLaSucesion()).thenReturn(new Float(107));
        when(sucesion2.sumatorioSucesion()).thenReturn(new Integer(1500));

        boolean resultado = OperadorDeSucesiones.mismoValor(sucesion1, sucesion2);

        assertEquals(resultado, true);
    }
}