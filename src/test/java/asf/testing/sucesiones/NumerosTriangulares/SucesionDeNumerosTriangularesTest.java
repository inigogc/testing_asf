package asf.testing.sucesiones.NumerosTriangulares;

import asf.testing.sucesiones.Excepciones.ElementoNoExisteException;
import asf.testing.sucesiones.Excepciones.SucesionDemasiadoPequenyaException;
import asf.testing.sucesiones.Fibonacci.SucesionDeFibonacci;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

public class SucesionDeNumerosTriangularesTest {
    
    @Test
    public void enLaPosicion3DeUnaSucesionDe5ElementosHayUn10() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(5);

        int valorObtenido = sucesionDeNumerosTriangulares.getElemento(3);

        assertEquals(10, valorObtenido);
    }

    @Test
    public void enLaPosicion10DeUnaSucesionDe12ElementosHayUn66() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(12);

        int valorObtenido = sucesionDeNumerosTriangulares.getElemento(10);

        assertEquals(66, valorObtenido);
    }

    @Test
    public void enLaPosicion0DeUnaSucesionDe2ElementosHayUn1() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(2);

        int valorObtenido = sucesionDeNumerosTriangulares.getElemento(0);

        assertEquals(1, valorObtenido);
    }

    @Test (expected= ElementoNoExisteException.class)
    public void enLaPosicion5DeUnaSucesionDe3SaltaExcepcion() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(3);

        int valorObtenido = sucesionDeNumerosTriangulares.getElemento(5);

        fail();
    }

    @Test (expected= SucesionDemasiadoPequenyaException.class)
    public void crearUnaSucesionDeTamanyo0GeneraExcepcion() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();

        sucesionDeNumerosTriangulares.inicializarSucesion(0);

        fail();
    }

    @Ignore
    @Test
    public void sumatorioDeSerieCon10ElementosDa88() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(10);

        int valorObtenido = sucesionDeNumerosTriangulares.sumatorioSucesion();

        assertEquals(88, valorObtenido);
    }


    @Ignore
    @Test
    public void mediaDeSerieCon10ElementosDa8con8() throws Exception {
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(10);

        Float valorObtenido = sucesionDeNumerosTriangulares.mediaDeLaSucesion();

        assertEquals(new Float(8.8), valorObtenido);
    }

    @Test
    public void valoresDeLaSucesionReturnsListWithCorrectSize() throws Exception{
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(10);

        List<Integer> valoresDeLaSucesion = sucesionDeNumerosTriangulares.valoresDeLaSucesion();

        assertThat( valoresDeLaSucesion, hasSize(10));
    }



    @Test
    public void valoresDeLaSucesionReturnsListWithCorrectSize2() throws Exception{
        SucesionDeNumerosTriangulares sucesionDeNumerosTriangulares = new SucesionDeNumerosTriangulares();
        sucesionDeNumerosTriangulares.inicializarSucesion(10);

        List<Integer> valoresDeLaSucesion = sucesionDeNumerosTriangulares.valoresDeLaSucesion();

        assertThat( valoresDeLaSucesion, hasItems(3, 15, 21));
    }
}